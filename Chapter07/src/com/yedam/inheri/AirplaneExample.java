package com.yedam.inheri;

public class AirplaneExample {

	public static void main(String[] args) {
		SuperSonicAirplane sa = new SuperSonicAirplane();
		
		sa.takeOff();
		
		//일반 모드로 가다가 비행모드를 바꿈.
		sa.fly();
		
		sa.flyMode = SuperSonicAirplane.SUPERSONIC;
		
		sa.fly();
		
		//
		
		//다시 비행보드를 일반모드로 바꿈.
		sa.flyMode = SuperSonicAirplane.NORMAR;
		
		sa.fly();
		
		sa.land();

	}

}
