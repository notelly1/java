package com.yedam.inheri;

public class Application {
	public static void main(String[] args) {
		Child child = new Child(); //Child 가 가지고 있는 필드 생성자 메소드를 쓸 수 있음
		
		child.lastName = "또치";
		child.age = 20;
		
		System.out.println("내 이름은 " + child.firstName + child.lastName);
		System.out.println("DNA는 " + child.DNA);
		//Parent 클래스 -> bloodType을 private로 설정
		//Child 클래스 -> Parent클래스의 bloodType 사용 X
		//System.out.println("혈액형은 " + child.bloodType);
		System.out.println("나이는 " + child.age);
		System.out.println();
		
		Child2 child2 = new Child2();
		
		child2.lastName = "희동";
		child2.age = 5;
		child2.bloodType = 'A';
		System.out.println("내 이름은 " + child2.firstName + child2.lastName);
		System.out.println("DNA는 " + child2.DNA);
		//Childe에 존재하는 bloodType을 사용 -> 상관X
		System.out.println("혈액형은 " + child2.bloodType);
		System.out.println("나이는 " + child2.age);
	}
}
