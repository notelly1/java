package com.yedam.inheri;

public class Child extends Parent{
	public String lastName;
	public int age;
	//성과 DNA 혈액형은 따로 선언하지 않아도 쓸 수 있다.
	
	
	//메소드
	//오버라이딩 예제
	//자바한테 미리 알려줌. ↓↓
	
	@Override
	public void method1() {
		System.out.println("child class -> method1 Override");
	}
	
	//상속 안받은 새로운 것
	public void method3() {
		System.out.println("child class -> method3 call");
	}
	//source에 들어가서 getter setter처럼 사용가능.
	
	
	
	
}
