package com.yedam.inheri;

public class Person extends People {
	
	public int age;
	//자식 객체를 만들 때, 생성자를 통해서 만든다.
	//super()를 통해서 부모 객체를 생성한다.
	//여기서 super() 의미하는 것은 부모의 생성자를 호출.
	//따라서 자식 객체를 만들게 되면 부모 객체도 같이 만들어진다.
	
	//Person 객체 오류가 남 why? 원래는 생성자 생성을 안하는데 이번에 해줌 // 원래는 기본 생성자 사용
	//기본 생성자가 아닌 People 부모의 생성자를 사용
	//자식 객체를 만들 때 부모 객체도 같이 만들어준다.
	//그걸 표기 해줘야 함 ↓↓
	public Person(String name, String ssn) {
		super(name, ssn);
	}
}
