package com.yedam.poly;

public class CarExample {
	public static void main(String[] args) {
		Car car = new Car();
		
		//5번 자동차 운행하겠다.
		for(int i = 0; i<=5; i++) {
			int problemLoc = car.run(); //class Car 의 메소드 run을 의미함.
			
			switch (problemLoc) {
			case 1:
				System.out.println("앞 왼쪽 HankookTire 교환");
				//Tire = 부모클래스(슈퍼클래스)
				//HankookTire = 자식클래스(서브클래스)
				//Tire frontLeftTire = new HankookTire("앞왼쪽",15);
				//부모기반 재정의된 자식클래스를 사용하겠다.
				car.frontLeftTire = new HankookTire("앞왼쪽", 15);
				break;
			case 2:
				System.out.println("앞 오른쪽 Kumho 교환");
				car.frontLeftTire = new KumhoTire("앞오른쪽", 15);
				break;
			case 3:
				System.out.println("뒤 왼쪽 HankookTire 교환");
				car.frontLeftTire = new HankookTire("뒤왼쪽", 15);			
				break;
			case 4:
				System.out.println("뒤 오른쪽 KumhoTire 교환");
				car.frontLeftTire = new KumhoTire("뒤오른쪽", 15);
				break;
			}
			System.out.println("==================================");
		}
	}
}
