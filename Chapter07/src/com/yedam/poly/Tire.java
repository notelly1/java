package com.yedam.poly;

public class Tire {
//타이어 수명이 다되면 자식클래스로 바꿔 끼우겠다는 예제
//차마다 맞는 규격이 따로 있다.
	
	// 필드
	public int maxRotation; //최대 회전수 (타이어 수명)
	public int accRotation; //누적 회전수 (현재까지의 회전 수)
	public String location; //타이어 위치
	//생성자
	public Tire(String location, int maxRotation) {
		this.location = location;
		this.maxRotation = maxRotation;
	}
	
	//메소드
	//타이어가 굴러가는 메소드
	public boolean roll() {
		++accRotation;
		if (accRotation < maxRotation) {
			System.out.println(location + "Tire 수명: " + (maxRotation-accRotation)+"회");
			return true;
		}else{
			System.out.println("#######" + location + "Tire 펑크" + "#######");
			return false;
		}
	}
	
	

	
	
}
