package com.yedam.poly;

public class DrawExample {

	public static void main(String[] args) {
		//자동타입변환
		//부모타입 변수 = new 자식 클래스()
		//부모클래스 하나의 객체로 여러 자식 클래스를 각각 다른 모습의 실행으로 나타내는 것.
		//이게 다형성
		Draw figure = new Draw();
		figure.draw();
		
		figure = new Circle();
		
		figure.x = 1;
		figure.x = 2;
		figure.draw();
		
		figure = new Rectangle();
		
		figure.draw();
		
		
	}

}
