package com.yedam.poly;

public class HankookTire extends Tire{

	public HankookTire(String location, int maxRotation) {
		super(location, maxRotation);
	}
	
	//메소드
	@Override
	public boolean roll() {
		++accRotation;
		if (accRotation < maxRotation) {
			System.out.println(location + "HankookTire 수명: " + (maxRotation-accRotation)+"회");
			return true;
		}else{
			System.out.println("#######" + location + "HankookTire 펑크" + "#######");
			return false;
		}
	}


}
