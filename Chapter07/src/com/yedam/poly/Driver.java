package com.yedam.poly;

public class Driver {
	
	//매개변수를 통한 자동변환
	
	public void drive(Vehicle vehicle) {
		vehicle.run();
	}
}
