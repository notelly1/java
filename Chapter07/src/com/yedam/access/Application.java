package com.yedam.access;

public class Application {
	public static void main(String[] args) {
		Child child = new Child();
		
		child.lastName = "길동";
		child.age = 50;
		
		//System.out.println("내 이름은 " + child.firstName + child.lastName);
		//System.out.println("DNA는 " + child.DNA);
		System.out.println("나이는 " + child.age);
		System.out.println();
		//부모가 요소에 protected 걸었을때
		child.showInfo();
	}
}
