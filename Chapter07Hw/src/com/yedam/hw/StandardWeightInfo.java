package com.yedam.hw;

public class StandardWeightInfo extends Human {
	//		- Human 클래스를 상속한다.

	public StandardWeightInfo(String name, int height, int weight) {
		super(name, height, weight);
	}

	//- 메소드는 다음과 같이 정의한다.
	//(1) public void getInformation() : 이름, 키, 몸무게와 표준체중을 출력하는 기능
	//표준체충은 없음.
	@Override
	public void getInfomation() {
		super.getInfomation();
		//1번 방식
		System.out.println("표준체중 : " + getStandardWeight());
		//2번 방식
		System.out.printf("표준체중 %.1f 입니다. \n", getStandardWeight());
	}
	
	//(2) public double getStandardWeight() : 표준체중을 구하는 기능
	//( * 표준 체중 : (Height - 100) * 0.9 )
	public double getStandardWeight() {
		double sw = (height - 100*0.9);
		//값을 다시 반환시켜준다.
		return sw;
	}

	
	
	
}
