package com.yedam.hw;

public class EmpDept extends Employee {
	//필드
	String deptName; //= "교육부";
	//생성자
	
	public EmpDept(){
		
	}	
	//생성자를 이용해 값을 초기화 한다.
	public EmpDept(String name, int payment, String deptName) {
		super(name, payment);
		this.deptName = deptName;
	}
	//추가된 필드 getter
	public String getDeptName() {
		return deptName;
	}
	
	
	//메소드
	@Override
	public void getInfomation() {
		//재사용성을 높이기 위해.
		//부모가 가진 메소드 실행 후 뒷부분만 추가.
		super.getInfomation();
		//println 엔터
		System.out.print(" 부서 : "+deptName);
		System.out.println();
	}
	
	@Override
	public void print() {
		super.print();
		System.out.println("서브클래스");
	}
}
