package com.yedam.hw;

public class Human {
	//-이름과 키, 몸무게를 필드로 가짐
	//필드
	String name;
	double height;
	double weight;
	//생성자
	//생성자를 이용해 값을 초기화 한다.
	public Human(String name, int height, int weight) {
	this.name =name;
	this.height = height;
	this.weight =weight;
	}
	
	//메소드
	//메소드는 다음과 같이 정의한다.
	//(1) public void getInformation() : 이름, 키와 몸무게를 출력하는 기능
	public void getInfomation() {
		System.out.println(name +"님의 신장 "+ height+", 몸무게 "+weight+"입니다.");

	}

	
}
