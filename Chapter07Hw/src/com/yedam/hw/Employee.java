package com.yedam.hw;

public class Employee {
	//필드
	String name; //= "이지나";
	int payment; //= 3000;
	//생성자
	public Employee() {
		
	}
	
	public Employee(String name, int payment) {
		this.name = name;
		this.payment = payment;
	}
	
	public String getName() {
		return name;
	}
	public int getPayment() {
		return payment;
	}	
	//메소드
	public void getInfomation() {
		System.out.print(" 이름 : " + name);
		System.out.print(" 연봉 : " + payment);
	}
	
	public void print() {
		System.out.println("슈퍼클래스");
	}
	
	
	
	
}
