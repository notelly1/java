package com.yedam.inter;

public class RCExample {
	public static void main(String[] args) {
		//인터페이스는 객체를 못 만들기 때문에
		//인터페이스를 사용하기 위해 변수를 만들어 준다.
		RemoteControl rc;
		
		//스스로가 객체를 만들지 못하기 때문에 자식을 불러와 객체화 시켜
		//자식이 오버라이드 한 메소드를 불러오는 것.
		rc = new SmartTV();
		//SmartTv 클래스 => implements RemoteControl( + Searchable) //동시에 하나의 인터페이스로 쓸 수 있는 것.
		//RemoteControl(+Searchable) -> Searchable 을 상속을 받고 있기 때문에
		//RemoteControl
		rc.turnOn();		
		rc.setVolume(40);
		rc.turnOff();
		System.out.println();
		//Searchable
		//search 메소드가 부모에게 없기 때문에 불러와서 쓸 수 없다.
		//따로 불러와서 써줘야한다.
		//Searchable sc = new SmartTV();
		//상속을 하게되면 쓸수 있는 듯.
		//수정하기
		rc.search("www.google.com");
		
		
		
		//rc = new Audio();
		
//		//오디오가 가진 메소드가 실행.
//		rc.turnOn();		
//		rc.setVolume(5);
//		rc.turnOff();
		
		//바로 객체를 만들지 않는 이유 >> 자식을 한번 거치는 이유
		//자기 자신을 객체를 만든 것 뿐.
		//뒤에 예제로 설명.
		//단독을 쓰는 것은 가능.
		Television tv = new Television();
		
		tv.turnOn();
		tv.setVolume(4);
		tv.turnOff();
		
	}
}
