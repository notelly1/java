package com.yedam.inter;

public class SmartTV implements RemoteControl, Searchable{
	//두개다 기능을 가지고 와서 넣어준다. 다중 인터페이스 구현
	//두가지 인터페이스를 가지고 와서 하나의 클래스에 집중시켜줄 수 있다.
	
	private int volume;
	
	//searchable
	@Override
	public void search(String url) {
		System.out.println(url + "을 검색합니다.");
		
	}

	@Override
	public void turnOn() {
		System.out.println("전원을 켭니다.");
		
	}

	@Override
	public void turnOff() {
		System.out.println("전원을 끕니다.");
		
	}

	@Override
	public void setVolume(int volume) {
		if(volume > RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
		}else if (volume < RemoteControl.MIN_VOLUME) {
			this.volume = RemoteControl.MIN_VOLUME;
		}else {
			this.volume = volume;
		}
		System.out.println("현재 볼륨 :  " + volume);
		
	}
	
}
