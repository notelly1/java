package com.yedam.inter;

public class LGWashingMachine implements WashingMachine{
	
	
	//세탁기 인터페이스에 드라이코스 인터페이스를 상속 시켜서 세탁기에 +드라이코스를 더함.
	@Override
	public void dry() {
		System.out.println("건조 코스 진행");		
	}

	@Override
	public void starBtn() {
		System.out.println("빨래 시작.");
		
	}

	@Override
	public void pauseBtn() {
		System.out.println("빨래 일시 중지.");
	}

	@Override
	public void stopBtn() {
		System.out.println("빨래 중지.");
		
	}

	@Override
	public void changeSpeed(int speed) {
		int nowSpeed = 0;
		switch (speed) {
		case 1:
			nowSpeed = 20;
			break;
		case 2:
			nowSpeed = 20;
			break;
		case 3:
			nowSpeed = 20;
			break;
		}
		
	}

}
