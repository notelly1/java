package com.yedam.inter;

public class Bird implements Animal{

	@Override
	public void walk() {
		System.out.println("걸을 수 있다.");
		
	}

	@Override
	public void fly() {
		System.out.println("날 수 있다.");
		
	}

	@Override
	public void sing() {
		System.out.println("노래할 수 있다.");
		
	}
	
}
