package com.yedam.inter;

public interface RemoteControl implements Searchable {
	
	//상수
	//불변의 값
	public static final int MAX_VOLUME =10;
	//final 생략
	public int MIN_VOLUME = 0;
	
	//추상 메소드
	//추상 메소드 안에 있는 것은 무조건 구현해야한다.
	//abstract 생략가능.
	public void turnOn(); //생략
	public abstract void turnOff(); //안 생략
	public void setVolume(int volume);
}
