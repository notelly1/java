package com.yedam.inter;

public class Television implements RemoteControl{
	//이건 일반 메소드
	
	//필드
	private int volume;
	//생성자
	
	//메소드
	@Override
	public void turnOn() {
		System.out.println("전원을 켭니다.");
		
	}

	@Override
	public void turnOff() {
		System.out.println("전원을 끕니다.");
		
	}

	@Override
	public void setVolume(int volume) {
		//데이터의 무결성을 지키기 위해 볼륨의 크기를 넘기지 않도록
		//최대소리 이상으로 데이터가 들어 올 때 
		if(volume > RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
		}else if (volume < RemoteControl.MIN_VOLUME) {
			this.volume = RemoteControl.MIN_VOLUME;
		}else {
			this.volume = volume;
		}
		System.out.println("현재 TV 볼륨 :  " + this.volume);
	}
	
}
