package com.yedam.inter;

public class Circle implements GetInfo{
	//필드
	int radius;
	
	//생성자
	public Circle(int radious) {
		this.radius =radius;
	}
	
	//메소드
	@Override
	public void area() {
		System.out.println("원의 넓이는 " + 3.14*radius*radius);		
	}

	@Override
	public void round() {
		System.out.println("원의 둘레는 " + 2*3.14*radius);
		
	}

	
}
