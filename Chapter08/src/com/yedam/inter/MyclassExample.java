package com.yedam.inter;

public class MyclassExample {

	public static void main(String[] args) {
		System.out.println("1) ===============");
		
		MyClass myClass = new MyClass();
		//.rc.를 쓰는 이유
		//MyClass안에 있는 rc를 부른다.
		//MyClass안에 있는 rc가 가지고 있는 메소드를 다 호출하는것. 
		//객체를 담고 있는 클래스를 인스턴스화 시킨 다음에 걔가 품고 있는 필드를 부른다.
		//필드는 자신이 가진 오버라이드 메소드를 부를 수 있다.

		myClass.rc.turnOn();
		myClass.rc.turnOff();
		
		System.out.println();
		System.out.println("2) ===============");
		
		//매개변수로 자식클래스를 넘겨주는 것
		//넘겨주게 되면, 
		//객체는 생성자를 통해 만들어지는데. 여기서는 그냥 만들어지는게 아니라 뉴오디오라는 객체를 넘겨준다.
		//생성자 오버로딩해서 만든거기 때문에 매개변수에 따라서 마이클래스 2)으로 간다.
		//오디오가 재정의한 턴온 턴오프가 실행된다.(둘다 오버라이딩 된 것들)
		//리모트컨트롤 안에 있는 메소드를 사용하는데 이것들은 다 오디오에 재정의한 메소드를 사용하겠다.
		MyClass myClass2 = new MyClass(new Audio());
		
		System.out.println();
		System.out.println("3) ===============");
		
		//method1을 찾아간다.
		//아래를 시작하는 리모트컨트롤 인터페이스를 사용하고 자식 오디오를 통해 실행한다.
		//오디오가 재정의한 턴온과 셋볼륨을 실행한다.
		//내가 원하는 자식 클래스를 사용한다. 로컬변수를 사용한다.
		MyClass myClass3 = new MyClass();
		myClass3.method1();
		
		System.out.println();
		System.out.println("4) ===============");
		
		
		//생성자 호출과 다를바 없으나...
		//리모트컨트롤에 정의한 메소드를 뉴텔레비전에 오버라이드 된 메소드를 가지고 와서 사용한다.
		//텔레비전이 재정의한 턴오프 턴온을 실행한다.
		//오버라이드 = 재정의
		//인터페이스를 하나만 넣고 내가 다른 자식클래스를 넣을 때 다 다른 값을 얻을 수 있다.
		//재정의한 값이 다르기 때문.
		// = 개발코드를 수정하지 않으면서 객체교환이 가능하다.
		
		MyClass myClass4 = new MyClass();
		myClass4.methodB(new Television());
		
		//인터페이스는~
		//객체가 아니라 자식이라고 생각하기.
		//더 쉽게 이해가능.
	}

}
