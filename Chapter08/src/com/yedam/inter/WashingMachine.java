package com.yedam.inter;

public interface WashingMachine extends DryCourse{
	public void starBtn();
	public void pauseBtn();
	public void stopBtn();
	public void changeSpeed(int speed);
}
