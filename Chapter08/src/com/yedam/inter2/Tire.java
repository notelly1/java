package com.yedam.inter2;

public interface Tire {
	//인터페이스에는 필드가 존재X
	//인터페이스의 상속을 받은 애들은 필드를 따로 구현해줘야한다.
	public void roll();
	
}
