package com.yedam.inter2;

public class CarExample {

	public static void main(String[] args) {
		Car myCar = new Car();
		
		myCar.run();
		System.out.println("====================");
		
		myCar.frontLeftTire = new KunhoTire();
		myCar.backRightTire = new HankookTire();
		myCar.run();
		
		//뭐라고 뭐라고...
		//자식클래스가 정의한 내용을 사용하게됨.
		//필드의 다양한 모습을 나타낸다 다향성
	}

}
