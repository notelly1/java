package com.yedam.inter2;

public class Car {
	Tire frontLeftTire = new HankookTire();
	Tire frontRightTire = new KunhoTire();
	Tire backLeftTire = new HankookTire();
	Tire backRightTire = new KunhoTire();
	
	public void run() {
		frontLeftTire.roll();
		frontRightTire.roll();
		backLeftTire.roll();
		backRightTire.roll();
	}
	
	
}
