package com.yedam.inter2;

public class Inheri {

	public static void main(String[] args) {
		// A <- B <- D( + A <- D)
		// A <- B
		A a = new B();
		a.info();
		
		A a2 = new D();
		a2.info();

	}

}
