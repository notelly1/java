package com.yedam.inter2;

public class Application {

	public static void main(String[] args) {
		Vehicle v1 = new Bus();
		/*drive(v1);
		
		Vehicle v2 = new Taxi();
		drive(v2);*/
		
		v1.run(); //자식 Bus에 있는 run을 가지고 온다.
		//v1.checkFare();//이건 자식에게만 있는 거기때문에 강제타입변환을 해줘야 쓸 수 있다.
		
		//Bus bus1 = new Bus();
		//Taxi taxi = (Taxi) bus1;
		
		//강제 타입 변환 => 자동 타입 변환이 선행되어야한다.
		
		//이것이 강제 타입 변환
		Bus bus = (Bus) v1;
		
		bus.run();
		bus.checkFare();
		
		drive(new Bus());
		drive(new Taxi());

	}
	
	// 원하는 조건의 자동 타입변환이 되었는지 확인후 강제 타입변환을 시켜줘야한다.
	// 대상이 누군지 잘 알아야하 한다.
	public static void drive (Vehicle vehicle) {
		if(vehicle instanceof Bus) {
			Bus bus = (Bus) vehicle;
			bus.checkFare();
		}
		vehicle.run();
	
	}

}
