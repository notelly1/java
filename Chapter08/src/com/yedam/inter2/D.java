package com.yedam.inter2;

public class D extends B {

	@Override
	public void info() {
		System.out.println("나는 D입니다.");

	}
	//오버라이드가 없으면
	//B값이 출력
	//상속의 개념 때문에 D가 B의 자식이고 B에 정의되 내용을 상속을 받기 때문에.
	//부모의 정보를 받아와 바로 사용.

}
