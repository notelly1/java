package com.yedam.inter2;

public class Example {

	public static void main(String[] args) {
		ImplementC impl = new ImplementC();
		
		InterfaceA ia = impl;
		ia.methodA();
		
		System.out.println();
		
		InterfaceB ib = impl;
		ib.methodB();
		
		System.out.println();
		
		//자기꺼 포함 A와 B꺼도 쓸수 있다.
		//인터페이스C는 A와 B의 자식. 그러므로 부모가 가진 메소드도 물려받았기 때문에 사용가능
		InterfaceC ic = impl;
		ic.methodA();
		ic.methodB();
		ic.methodC();
	}

}
